FROM public.ecr.aws/docker/library/alpine:3.21.3

# Update and upgrade repositories
RUN apk update \
  && apk upgrade

# Install basic tools
RUN apk add --no-cache \
  bash \
  coreutils \
  vim \
  ca-certificates \
  curl \
  wget \
  git \
  jq \
  yq

# Install Python
RUN apk add --no-cache \
  python3 \
  py3-pip \
  && ln -sf python3 /usr/bin/python \
  && ln -sf pip3 /usr/bin/pip

# Install python pipx
RUN apk add --no-cache \
  pipx \
  && echo 'export PATH=$HOME/.local/bin:$PATH' >> /etc/profile

# Install python package manager pipenv
RUN pipx install pipenv

# Setup zsh
RUN apk add --no-cache \
  zsh \
  && sh -c "$(curl -L https://github.com/deluan/zsh-in-docker/releases/download/v1.2.1/zsh-in-docker.sh)" \
  && sed -i s#/bin/sh#/bin/zsh#g /etc/passwd

# Install network tools
RUN apk add --no-cache \
  bind-tools \
  iperf \
  iproute2 \
  iputils \
  tcpdump \
  tshark \
  ipcalc

# Install arp-scan from edge community repository
RUN apk add --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community/ --no-cache \
  arp-scan

# Clean up APK cache
RUN rm -rf /var/cache/apk/*

CMD ["/bin/zsh", "--login"]
