# Nettools

Description of the container

[[_TOC_]]

## Usage

Pull container from GitLab Container Registry:

```shell
docker pull registry.gitlab.com/mkuurstra-works/containers/nettools:v1.0.0 # version
```

## Credits

This project was created with Cookiecutter and the [mkuurstra-works/cookiecutter/cookiecutter-container-builder](https://gitlab.com/mkuurstra-works/cookiecutter/cookiecutter-container-builder) project template.

### Acknowledgements

Inspired by:

- <https://github.com/wbitt/Network-MultiTool>
- <https://github.com/jonlabelle/docker-network-tools>
- <https://github.com/deluan/zsh-in-docker>
